import scrapy
import pandas as pd
from urllib.parse import urlparse



class logocrawler(scrapy.Spider):
    #handle_httpstatus_list = [403]
    name = "logo"
    input_csv_file = 'Links.csv'

    def clean_url(self, url):
        url = url.replace("['", "")
        url = url.replace("']", "")
        url = url.replace('["', "")
        url = url.replace('"]', "")
        return url

    def start_requests(self):
        df = pd.read_csv(self.input_csv_file, header=None, names = ['urls'])
        urls = list(df.urls)
        urls[:] = [f'http://{el}' for el in urls]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        print("\n\n<<<< Starting >>>>")

        str_url = str(response.url).lower()
        parsed_url = urlparse(str_url)
        clean_url = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_url)
        print(clean_url)

        img_url_list = []
        url_list = []
        case_list = []

        ### Case 1:
        CHECK = False
        for tag_a in response.xpath('//a'):
            a_id = str(tag_a.xpath('@id').extract())
            for tag_img in tag_a.xpath('.//img'):
                img_url = str(tag_img.xpath('@src').extract())
                img_url = self.clean_url(img_url)
                img_class = str(tag_img.xpath('@class').extract())
                img_alt = str(tag_img.xpath('@alt').extract())
                img_title = str(tag_img.xpath('@title').extract())
                img_id = str(tag_img.xpath('@id').extract())
                ind_list = [img_url.lower().find('logo'),
                            img_class.lower().find('logo'),
                            img_alt.lower().find('logo'),
                            img_title.lower().find('logo'),
                            img_id.lower().find('logo'),
                            a_id.lower().find('logo')]
                if any(ind > -1 for ind in ind_list):
                    CHECK = True
                    img_url_list.append(img_url)
                    url_list.append(clean_url)
                    case_list.append('1')
        ### Case 2:
        if not CHECK:
            for tag_div in response.xpath('//div'):
                for tag_img in tag_div.xpath('.//img'):
                    img_url = str(tag_img.xpath('@src').extract())
                    img_url = self.clean_url(img_url)
                    img_class = str(tag_img.xpath('@class').extract())
                    img_alt = str(tag_img.xpath('@alt').extract())
                    img_title = str(tag_img.xpath('@title').extract())
                    ind_list = [img_url.lower().find('logo'),
                                img_class.lower().find('logo'),
                                img_alt.lower().find('logo'),
                                img_title.lower().find('logo')]
                    if any(ind > -1 for ind in ind_list):
                        CHECK = True
                        img_url_list.append(img_url)
                        url_list.append(clean_url)
                        case_list.append('2')

        ### Case 3:
        if not CHECK:
            for tag_head in response.xpath('//head'):
                for tag_img in tag_head.xpath('.//meta'):
                    img_url = str(tag_img.xpath('@content').extract())
                    img_url = self.clean_url(img_url)
                    ind = img_url.lower().find('logo')
                    if ind > 0:
                        CHECK = True
                        img_url_list.append(img_url)
                        url_list.append(clean_url)
                        case_list.append('3')

        ### Case 4:
        if not CHECK:
            for tag_head in response.xpath('//head'):
                for tag_img in tag_head.xpath('.//link'):
                    img_url = str(tag_img.xpath('@href').extract())
                    img_url = self.clean_url(img_url)
                    ind_list = [img_url.lower().find('favicon'),
                                img_url.lower().find('logo')]
                    if any(ind > -1 for ind in ind_list):
                        img_url_list.append(img_url)
                        url_list.append(clean_url)
                        case_list.append('4')

            ### Case 5:
            for tag_body in response.xpath('//body'):
                for tag_img in tag_body.xpath('.//link'):
                    img_url = str(tag_img.xpath('@href').extract())
                    img_url = self.clean_url(img_url)
                    ind_list = [img_url.lower().find('favicon'),
                                img_url.lower().find('logo')]
                    if any(ind > -1 for ind in ind_list):
                        img_url_list.append(img_url)
                        url_list.append(clean_url)
                        case_list.append('5')

            ### Case 6:
            for tag_body in response.xpath('//body'):
                for tag_img in tag_body.xpath('.//meta'):
                    img_url = str(tag_img.xpath('@content').extract())
                    img_url = self.clean_url(img_url)
                    ind_list = [img_url.lower().find('favicon'),
                                img_url.lower().find('logo')]
                    if any(ind > -1 for ind in ind_list):
                        img_url_list.append(img_url)
                        url_list.append(clean_url)
                        case_list.append('6')

        data = {'img_url_list':img_url_list, 'url_list':url_list, 'case_list':case_list}

        for i in range(len(url_list)):
            parsed_url = urlparse(url_list[i])
            prot = '{uri.scheme}:'.format(uri=parsed_url)

            if img_url_list[i][:2] == '//':
                img_url_list[i] = str(prot) + str(img_url_list[i])
            if img_url_list[i][0] == '/' and img_url_list[i][1] != '/':
                img_url_list[i] = str(url_list[i]) + str(img_url_list[i])
            if img_url_list[i][0] != '/' and img_url_list[i][0] != "[" and img_url_list[i][:4] != 'http':
                img_url_list[i] = str(url_list[i]) + "/" + str(img_url_list[i])
            # if img_url_list[i].find('[]') > -1 or img_url_list[i].find('data:') > -1:
            #     del img_url_list[i]
            #     del url_list[i]
            #     del case_list[i]

        df1 = pd.DataFrame(data)
        df1 = df1.drop_duplicates()
        print(df1)
        df1.to_csv('MY_URLS_1.csv', mode='a', index=False, header=False, sep='\t')

        print("<<<< Ending >>>>\n\n")
